
$(function() {



// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown').hover(function() {
    $(this).children('.dropdown-menu').stop(true,true).fadeToggle();
   })
}else {
  $('.dropdown').click(function() {
    $(this).children('.dropdown-menu').stop(true,true).fadeToggle();
   })
}



 // Банер

 $('.banner').slick({
  dots: true,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  // autoplay: true,
  // autoplaySpeed: 5000,
});


// Показать(скрыть) блоки
$('.advantages-show__btn').click(function() {
  $(this).toggleClass('active');
  $('.advantages-wrap__row--hidden').fadeToggle();
})


// FansyBox
 $('.fancybox').fancybox({});


 // Слайдер лицензий
 $('.licenses-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
});

// Сллайдер сертификатов
$('.brands-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 7,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 5,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 2
      },
      
    },
    {
      breakpoint: 479,
    settings: {
      slidesToShow: 2
      }
    },
  ]
});



// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.header').toggleClass("active");
  $('.main-menu').fadeToggle(200);
});





// Меню в сайдбаре
$('.sidebar-menu >li >a').click(function() {
  $(this).toggleClass('.active');
  $(this).next('ul').fadeToggle();
})


// Регулируем положение точек на главном слайдере
if ($(window).width() < 480) {

  var imgHeight = $('.banner-item > img').height();
  $('.slick-dots').css('top', imgHeight-30 + "px");
    
   $(window).on('resize', function(){
      console.log($(window).width())
      var imgHeight = $('.banner-item > img').height();
      $('.slick-dots').css('top', imgHeight-30 + "px");
  });
}


// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})




})